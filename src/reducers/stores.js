import { FETCH_STORES } from "../actions/types";

const storesReducer = (state = [], action) => {
    switch(action.type) {
        case FETCH_STORES: 
            const stores = action.payload.data?.map(store => store)
            console.log('stores here', stores)
            return [
                ...state,
                ...stores
            ]
        default:
            return state
    }
}

export default storesReducer