import { combineReducers } from "redux";
import storesReducer from './stores'

const reducers = combineReducers({
    stores: storesReducer
})

export default reducers