import { useState } from 'react'

const AddStore = () => {
	const [department, setDepartment] = useState('')
	const [city, setCity] = useState('')
	const [availableCities, setAvailableCities] = useState([])

	const onDepartmentChange = (event) => {
		if (event.target.value === 'DC') {
			setAvailableCities([
				{
					text: 'Bogotá',
					value: 'BOG'
				}
			])
		} else if (event.target.value === 'RIS') {
			setAvailableCities([
				{
					text: 'Pereira',
					value: 'PEI'
				}
			])
		} else {
			setAvailableCities([])
		}
		setDepartment(event.target.value)
		console.log(department)
	}

	const onCityChange = (event) => {
		setCity(event.target.value)
		console.log('city here', city)
	}

	return (
		<>
			<form>
				<div className='form-group row'>
					<input type='text' className='form-control col' placeholder='Nombre' />
					<textarea className='form-control col' placeholder='Descripción' rows='3'></textarea>
				</div>
				<div className='form-group row'>
					<input type='email' className='form-control col' placeholder='Email' />
					<input type='number' className='form-control col' placeholder='Teléfono' />
				</div>
				<div className='form-group row'>
					<input type='text' className='form-control col' placeholder='Dirección' />
					<select multiple className='form-control col' onChange={onDepartmentChange}>
						<option value=''>Departamento</option>
						<option value='DC'>Bogotá D.C.</option>
						<option value='RIS'>Risaralda</option>
					</select>
					<select multiple className='form-control col' onChange={onCityChange}>
						<option value=''>Ciudad</option>
						{availableCities?.map((city) => {
							return (
								<option key={city.value} value={city.value}>
									{city.text}
								</option>
							)
						})}
					</select>
				</div>
			</form>
		</>
	)
}

export default AddStore
