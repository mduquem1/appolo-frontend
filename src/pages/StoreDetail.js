import React from 'react'
import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'

import ButtonPrimary from 'components/ButtonPrimary'

const StoreDetail = () => {
	let { id } = useParams()
	const stores = useSelector((state) => state.stores)
	const store = stores.filter((store) => store.id.toString() === id)[0]
	return (
		<div className='card m-2'>
			<img className='card-img-top' src={store.photos[0]} alt={store.name} />
			<div className='card-body'>
				<h5 className='card-title'>{store.name}</h5>
				<p className='card-text'>{store.description}</p>
				<p className='card-text'>
					{store.address} {store.city} {store.department}
				</p>
				<p className='card-text'>{store.email}</p>
				<p className='card-text'>Registrado desde: {store.registeredSince}</p>
				<ButtonPrimary onClickHandler={() => console.log('contratar servicio')}>
					Contratar servicio
				</ButtonPrimary>
			</div>
		</div>
	)
}

export default StoreDetail
