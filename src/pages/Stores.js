import React, { useEffect } from 'react'
import StoresList from 'components/StoresList'
import { useSelector, useDispatch } from 'react-redux'
import { fetchStores } from 'actions'
const Stores = ({ openDetailHandler }) => {
	const dispatch = useDispatch()
	useEffect(() => {
		dispatch(fetchStores())
	}, [dispatch])
	const stores = useSelector((state) => state.stores)

	return (
		<div>
			<StoresList stores={stores} openDetailHandler={openDetailHandler} />
		</div>
	)
}

export default Stores
