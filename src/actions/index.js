import axios from 'axios'
import { BASE_URL } from 'constants/url'

import { FETCH_STORES } from "./types"

export const fetchStores = () => {
    const response = axios.get(BASE_URL + '/stores')
    console.log('response', response)
    return {
        type: FETCH_STORES,
        payload: response
    }
}