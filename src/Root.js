import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reduxPromise from 'redux-promise'
import { BrowserRouter as Router } from 'react-router-dom'

import reducers from 'reducers'

const Root = ({ children, initialState = {} }) => {
	const store = createStore(reducers, initialState, applyMiddleware(reduxPromise))
	return (
		<Router>
			<Provider store={store}>{children}</Provider>
		</Router>
	)
}

export default Root
