import { Routes, Route, useNavigate, useLocation } from 'react-router-dom'

import Stores from 'pages/Stores'
import StoreDetail from 'pages/StoreDetail'
import AddStore from 'pages/AddStore'
import ButtonPrimary from 'components/ButtonPrimary'
import ButtonInfo from 'components/ButtonInfo'


const App = () => {
	const navigate = useNavigate()
	const location = useLocation()

	const onDetailClick = (id) => {
		navigate(`/store/${id}`)
	}

	return (
		<div className='container my-4'>
			<header className='d-flex justify-content-around my-4'>
				{location.pathname !== '/' && <ButtonInfo onClickHandler={() => navigate(-1)}>Ir atrás</ButtonInfo>}
				<h1>Appollo</h1>
				<ButtonPrimary onClickHandler={() => navigate('/add-store')}>
					Añadir establecimiento
				</ButtonPrimary>
			</header>
			<main className='container'>
			<Routes>
				<Route exact path='/' element={<Stores openDetailHandler={onDetailClick} />} />
				<Route path='/store/:id' element={<StoreDetail />} />
				<Route path='/add-store' element={<AddStore />} />
			</Routes>
			</main>
			<footer>
				Footer
			</footer>
		</div>
	)
}

export default App
