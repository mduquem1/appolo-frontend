import ReactDOM from 'react-dom'

import Root from 'Root'
import App from 'App'

import 'bootstrap/dist/css/bootstrap.css'

ReactDOM.render(
    <Root>
        <App />
    </Root>,
    document.querySelector('#root')
)