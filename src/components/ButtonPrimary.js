const ButtonPrimary = ({ children, onClickHandler }) => {
	return (
		<button className='btn btn-primary' onClick={onClickHandler}>
			{children}
		</button>
	)
}

export default ButtonPrimary
