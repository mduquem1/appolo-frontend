const ButtonInfo = ({ children, onClickHandler }) => {
	return (
		<button className='btn btn-info' onClick={onClickHandler}>
			{children}
		</button>
	)
}

export default ButtonInfo
