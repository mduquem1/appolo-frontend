import StoreCard from './StoreCard'

const StoresList = ({stores, openDetailHandler}) => {
	return (
		<div className='row'>
			{stores?.map((store) => (
				<StoreCard store={store} key={store.id} onDetailClick={openDetailHandler} />
			))}
		</div>
	)
}

export default StoresList
