import ButtonPrimary from './ButtonPrimary'

const StoreCard = ({ store, onDetailClick }) => {
	return (
		<div className='card col-md-2 m-2'>
			<img className='card-img-top' src={store.photos[0]} alt={store.name} />
			<div className='card-body'>
				<h5 className='card-title'>{store.name}</h5>
				<p className='card-text'>{store.description}</p>
				<ButtonPrimary onClickHandler={() => onDetailClick(store.id)}>Conocer más</ButtonPrimary>
			</div>
		</div>
	)
}

export default StoreCard
